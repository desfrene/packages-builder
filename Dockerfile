FROM debian:latest

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get -y update
RUN apt-get -y upgrade

RUN apt-get -y install git-buildpackage dh-make sudo nano

RUN apt-get clean

RUN echo "debian ALL=(ALL:ALL) NOPASSWD: /usr/bin/apt-get"  | sudo tee /etc/sudoers.d/debian-apt-get

ARG USER=debian
SHELL ["/bin/bash", "-c"]

RUN groupadd -g 1000 -r $USER
RUN useradd -rm -d /home/$USER -s /bin/bash -G sudo -u 1000 -g 1000 $USER
RUN echo $USER:debian | chpasswd

RUN mkdir "/src"
RUN chown -R debian:debian /src

USER $USER

RUN sed -i 's/#force_color_prompt/force_color_prompt/g' /home/$USER/.bashrc
RUN sed -i 's/#alias/alias/g' /home/$USER/.bashrc
RUN sed -i 's/#export/export/g' /home/$USER/.bashrc
RUN sed -i 's/ls -l/ls -al/g' /home/$USER/.bashrc


RUN git config --global user.name "Gabriel Desfrene"
RUN git config --global user.email desfrene.gabriel@gmail.com

ENV DEBEMAIL "desfrene.gabriel@gmail.com"
ENV DEBFULLNAME "Gabriel Desfrene"

WORKDIR /src
ENTRYPOINT ["/bin/bash"]
