#!/usr/bin/env bash

OLD_DIR="$(pwd)"
cd "$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)" || exit 5

TO_UPLOAD="$1"

echo "Working with $TO_UPLOAD files"
echo


if [ "$(find . -maxdepth 1 -name "$TO_UPLOAD\\_*" -print | wc -w)" -lt 3 ]; then
  echo "No enough file to work with..."
  exit 1
fi

CHANGE_FILE="$(find . -maxdepth 1 -name "$TO_UPLOAD\\_*.changes" -print)"

if [ ! -f "$CHANGE_FILE" ]; then
  echo "Changes file not found."
  exit 1
fi


echo "Change file found : $CHANGE_FILE"
echo

echo "Signing build..."
echo
debsign "$CHANGE_FILE"

echo
echo "Uploading to debsign.desfrene.fr..."
echo

dput desfrene "$CHANGE_FILE"

echo
echo "Cleaning..."
echo

find . -maxdepth 1 -name "$TO_UPLOAD\\_*" -exec rm {} +

cd "$OLD_DIR" || exit 1

echo "Done"
