#!/usr/bin/env bash

OLD_DIR="$(pwd)"
cd "$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)" || exit 5

UPSTREAM_BRANCH="upstream"
DEBIAN_BRANCH="debian"


if [ ! -d "$1" ]; then
    echo "Git project detected !"
    echo

    GIT_PROJECT=1
    REPO_GIT_URL="$1"

    echo "Repository URL : $REPO_GIT_URL"
    echo

    REPO_DIR="$(echo "$REPO_GIT_URL" | rev | cut -d '/' -f 1 | rev | cut -d '.' -f 1)"
else
    echo "Local project detected !"
    echo

    GIT_PROJECT=0
    REPO_DIR="$1"
fi

echo "Directory : $REPO_DIR"
echo

if [ ! -d "$REPO_DIR" ]; then
  echo "Installing new repository : $REPO_DIR"
  mkdir -p "$REPO_DIR/debian"
  exit 0
fi

cd "$REPO_DIR" || exit 2

find . -maxdepth 1 ! -name 'debian' ! -name '.' ! -name '..' -exec rm -rf {} +

git init --initial-branch="$DEBIAN_BRANCH"

echo
echo

git add debian/*
git commit -m "Commit"

echo
echo

if [ $GIT_PROJECT -eq 1 ]; then
    git remote add "$UPSTREAM_BRANCH" "$REPO_GIT_URL"
    git fetch "$UPSTREAM_BRANCH"

    echo
    echo

    FULL_UPSTREAM_BRANCH="$(git branch -r | xargs)"
    git rebase "$FULL_UPSTREAM_BRANCH"
else
    git checkout -b "$UPSTREAM_BRANCH"
    git checkout "$DEBIAN_BRANCH"
fi

echo
echo

cd ..

DOCKER_INSTANCE="$(docker run -d -t -v "$(pwd)":/src debuilder)"
echo "Docker instance : $DOCKER_INSTANCE"
echo

DEB_VERSION="$(docker exec -w "/src/$REPO_DIR" "$DOCKER_INSTANCE" bash -c "dpkg-parsechangelog --show-field Version | cut -d '-' -f 1")"
echo "Version : $DEB_VERSION"
echo
docker exec -w "/src/$REPO_DIR" "$DOCKER_INSTANCE" git tag "v$DEB_VERSION"

MISSING_PACKAGES="$(docker exec -w "/src/$REPO_DIR" "$DOCKER_INSTANCE" bash -c "dpkg-checkbuilddeps 2>&1 >/dev/null | cut -d ':' -f 4 | xargs")"
echo "Missing Packages : '$MISSING_PACKAGES'"
echo
docker exec -w "/src/$REPO_DIR" "$DOCKER_INSTANCE" bash -c "DEBIAN_FRONTEND=noninteractive sudo apt-get install -y $MISSING_PACKAGES"

echo
echo

docker exec -w "/src/$REPO_DIR" "$DOCKER_INSTANCE" bash -c "gbp buildpackage --git-pristine-tar --git-pristine-tar-commit --git-upstream-tag='v%(version)s' --git-debian-branch=$DEBIAN_BRANCH --git-upstream-branch=$FULL_UPSTREAM_BRANCH --no-sign"

echo
echo

echo "Stopping container"
echo
docker stop -t 1 "$DOCKER_INSTANCE" >/dev/null
docker rm "$DOCKER_INSTANCE" >/dev/null

echo "Cleaning..."
cd "$REPO_DIR" || exit 2
git clean -fdx
find . -maxdepth 1 ! -name 'debian' ! -name '.' ! -name '..' -exec rm -rf {} +
cd ..
echo

cd "$OLD_DIR" || exit 1

echo "Done"
